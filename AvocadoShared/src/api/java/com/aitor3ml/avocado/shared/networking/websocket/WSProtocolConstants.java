package com.aitor3ml.avocado.shared.networking.websocket;

public interface WSProtocolConstants {

	public static final int AVOCADO_SERIALIZABLE = 1;

	public static final int JAVA_SERIALIZED = 2;

}
